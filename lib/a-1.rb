# Given an array of unique integers ordered from least to greatest, write a
# method that returns an array of the integers that are needed to
# fill in the consecutive set.

def missing_numbers(nums)
  low = nums[0]
  high = nums[-1]
  missing = []

  (low..high).each do |num|
    missing << num unless nums.include?(num)
  end
  missing
end

# Write a method that given a string representation of a binary number will
# return that binary number in base 10.
#
# To convert from binary to base 10, we take the sum of each digit multiplied by
# two raised to the power of its index. For example:
#   1001 = [ 1 * 2^3 ] + [ 0 * 2^2 ] + [ 0 * 2^1 ] + [ 1 * 2^0 ] = 9
#
# You may NOT use the Ruby String class's built in base conversion method.

def base2to10(binary)
  array = binary.to_s.chars.map {|el| el.to_i}
  newarray = []

  array.reverse.each_with_index do |num, idx|
    newarray << (num * (2 ** idx))
  end
  newarray.reduce(:+)
end

class Hash

  # Hash#select passes each key-value pair of a hash to the block (the proc
  # accepts two arguments: a key and a value). Key-value pairs that return true
  # when passed to the block are added to a new hash. Key-value pairs that return
  # false are not. Hash#select then returns the new hash.
  #
  # Write your own Hash#select method by monkey patching the Hash class. Your
  # Hash#my_select method should have the functionailty of Hash#select described
  # above. Do not use Hash#select in your method.

  def my_select(&prc)
    new_hash = Hash.new

    self.each do |k,v|
      new_hash[k] = v if prc.call(k,v)
    end
    new_hash
  end
end

class Hash

  # Hash#merge takes a proc that accepts three arguments: a key and the two
  # corresponding values in the hashes being merged. Hash#merge then sets that
  # key to the return value of the proc in a new hash. If no proc is given,
  # Hash#merge simply merges the two hashes.
  #
  # Write a method with the functionality of Hash#merge. Your Hash#my_merge method
  # should optionally take a proc as an argument and return a new hash. If a proc
  # is not given, your method should provide default merging behavior. Do not use
  # Hash#merge in your method.

  def my_merge(hash, &prc)
    result = Hash.new(0)
    result = self.clone
    hash.each do |k2, v2|
      result[k2] = v2
    end

    if prc != nil
      res = {}
      result.each do |key,new_val|
        old_val = nil
         if self.keys.include?(key)
           old_val = self[key]
         else
           old_val = 0
         end
         if self.keys.include?(key) and hash.keys.include?(key)
           res[key] = prc.call(key,old_val,new_val)
         else
           res[key] = new_val
         end
      end
      return res
    end
   result
  end
end

# The Lucas series is a sequence of integers that extends infinitely in both
# positive and negative directions.
#
# The first two numbers in the Lucas series are 2 and 1. A Lucas number can
# be calculated as the sum of the previous two numbers in the sequence.
# A Lucas number can also be calculated as the difference between the next
# two numbers in the sequence.
#
# All numbers in the Lucas series are indexed. The number 2 is
# located at index 0. The number 1 is located at index 1, and the number -1 is
# located at index -1. You might find the chart below helpful:
#
# Lucas series: ...-11,  7,  -4,  3,  -1,  2,  1,  3,  4,  7,  11...
# Indices:      ... -5, -4,  -3, -2,  -1,  0,  1,  2,  3,  4,  5...
#
# Write a method that takes an input N and returns the number at the Nth index
# position of the Lucas series.

def lucas_numbers(n)
  hash = {}
  hash[-1] = -1
  hash[0] = 2
  hash[1] = 1
  if n > 1
    (2..n).each do |key|
      hash[key] = hash[key-1] + hash[key-2]
    end
  elsif n < -1
    (n..-2).to_a.reverse.each do |key|
      if key.even?
        hash[key] = hash[key+1].abs + hash[key+2].abs
      else
        hash[key] = -(hash[key+1].abs + hash[key+2].abs)
      end
    end
  end
  hash[n]
end

# A palindrome is a word or sequence of words that reads the same backwards as
# forwards. Write a method that returns the longest palindrome in a given
# string. If there is no palindrome longer than two letters, return false.

def longest_palindrome(string)
  array = string.chars
  palin_length = []

  array.each_index do |idx1|
    array.each_index do |idx2|
      next if idx1 == idx2
      if (array[idx1..idx2] == array[idx1..idx2].reverse)
        palin_length << array[idx1..idx2].length
      end
    end
  end
  if palin_length.max == 0
    return false
  else
    palin_length.max
  end
end
